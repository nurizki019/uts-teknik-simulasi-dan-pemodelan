/**
* Keterangan Variable dalam Function:
* - a = konstanta pengali
* - c = konstanta pergeseran
* - m = modulus
* - zn = nilai awal
* - n = jumlah bilangan acak yang dihasilkan
*/
function randomNumberGenerator(a, c, m, zn, n) {
  let arr = [];
  for (let i = 0; i < n; i++) {
  zn = (a * zn + c) % m;
  arr.push(zn);
  }
  return arr;
  }
  /**
  * Keterangan Variable dalam Function:
* - rerata = nilai rata-rata
* - nilaiSingle = array yang berisi bilangan acak
*/
function perhitunganVariant(rerata, nilaiSingle) {
  let hasil = 0;
  for (let i = 0; i <= nilaiSingle.length - 1; i++) {
  hasil += Math.pow(nilaiSingle[i] - rerata, 2);
  }
  return hasil / (nilaiSingle.length - 1);
  }
  /**
  * Keterangan Variable dalam Function:
  * - name = nama nilai
  * - nilaiSingle = array yang berisi bilangan acak
  */
  function perhitunganStDev(name, nilaiSingle) {
  let jumlah = 0;
  let arr = [];
  for (let i = 0; i <= nilaiSingle.length - 1; i++) {
  jumlah += nilaiSingle[i];
  }
  let rataRata = jumlah / nilaiSingle.length;
  let variant = perhitunganVariant(rataRata, nilaiSingle);
  let stDev = Math.sqrt(variant);
  // Menampilkan hanya 3 angka di belakang koma
  stDev = parseFloat(stDev.toFixed(3));
  arr.push({
  name,
  jumlah,
  rataRata,
  variant,
  stDev,
  });
  return arr;
}
/**
* Keterangan Variable dalam Function:
* - nilai = array yang berisi bilangan acak
* - bobot = array yang berisi bobot
*/
function perhitunganNilai(nilai, bobot) {
let arr = [];
for (let i = 0; i <= nilai.affinity_v.length - 1; i++) {
let nilaiAkhiraffinity = nilai.affinity_v[i] * (bobot.affinity_w / 100);
let nilaiAkhirPhotoshop = nilai.Photoshop_v[i] * (bobot.Photoshop_w / 100);
let nilaiAkhirfigma = nilai.figma_v[i] * (bobot.figma_w / 100);
let nilaiAkhirCorelDraw = nilai.CorelDraw_v[i] * (bobot.CorelDraw_w / 100);
let nilaiAkhir =
nilaiAkhiraffinity + nilaiAkhirPhotoshop + nilaiAkhirfigma + nilaiAkhirCorelDraw;
let hasilAkhir = "";
if (nilaiAkhir >= 100) {
hasilAkhir = "Sempurna";
} else if (nilaiAkhir >= 81) {
  hasilAkhir = "Bagus";
} else if (nilaiAkhir >= 70) {
hasilAkhir = "Cukup";
} else if (nilaiAkhir >= 60) {
hasilAkhir = "Ditingkatkan lagi";
} else if (nilaiAkhir >= 50) {
hasilAkhir = "Baru Belajar";
} else {
hasilAkhir = "Tidak Tahu";
}
arr.push({
AplikasiEditing: `AplikasiEditing ${i + 1}`,
perhitungan: {
nilaiAFFINITY: nilai.affinity_v[i],
bobotAFFINITY: bobot.affinity_w,
nilaiAkhirAFFINITY: Math.round(nilaiAkhiraffinity),
nilaiPHOTOSHOP: nilai.Photoshop_v[i],
bobotPHOTOSHOP: bobot.Photoshop_w,
nilaiAkhirPHOTOSHOP: Math.round(nilaiAkhirPhotoshop),
nilaifIGMA: nilai.figma_v[i],
bobotfIGMA: bobot.figma_w,
nilaiAkhirFIGMA: Math.round(nilaiAkhirfigma),
nilaiCORELDRAW: nilai.CorelDraw_v[i],
bobotCORELDRAW: bobot.CorelDraw_w,
nilaiAkhirCORELDRAW: Math.round(nilaiAkhirCorelDraw),
},
hasilAkhir: hasilAkhir,
});
}
return arr;
}
function nilaiFilterCount(hasil, hasilAkhir) {
let hasilCount = 0;
for (let i = 0; i <= hasil.length - 1; i++) {
if (hasil[i].hasilAkhir === hasilAkhir) {
hasilCount++;
}
}
return hasilCount;
}
// random number generator untuk affinity (First Contentful Paint)
let a = 5;
let c = 7;
let m = 100;
let zn = 5;
let n = 20;
let affinity = randomNumberGenerator(a, c, m, zn, n);
// random number generator untuk Photoshop (Largest Contentful Paint)
a = 2;
c = 7;
m = 100;
zn = 14;
n = 20;
let Photoshop = randomNumberGenerator(a, c, m, zn, n);

// random number generator untuk figma (Total Blocking Time)
a = 2;
c = 2;
m = 100;
zn = 4;
n = 20;
let figma = randomNumberGenerator(a, c, m, zn, n);

// random number generator untuk CorelDraw (Cumulative Layout Shift)
a = 2;
c = 9;
m = 100;
zn = 18;
n = 20;
let CorelDraw = randomNumberGenerator(a, c, m, zn, n);
let bobot = {
affinity_w: 90,
Photoshop_w: 25,
figma_w: 18,
CorelDraw_w: 80,
};

let nilai = {
  affinity_v: affinity,
  Photoshop_v: Photoshop,
  figma_v: figma,
  CorelDraw_v: CorelDraw,
  };

  console.log("==================================");
console.log("Mohamad Nurizki");
console.log("Kemahiran aplikasi AplikasiEditing yang digunakan");
console.log("==================================");
console.table(perhitunganStDev("AFFINITY`", affinity));
console.table(perhitunganStDev("PHOTOSHOP", Photoshop));
console.table(perhitunganStDev("FIGMA", figma));
console.table(perhitunganStDev("CORELDRWAR", CorelDraw));
console.log("==================================");
let hasil = perhitunganNilai(nilai, bobot);
console.log(hasil);
console.log("==================================");
console.log("Hasil Simulasi: ");
console.log(`A = ${nilaiFilterCount(hasil, "A")}`);
console.log(`B = ${nilaiFilterCount(hasil, "B")}`);
console.log(`C = ${nilaiFilterCount(hasil, "C")}`);
console.log(`D = ${nilaiFilterCount(hasil, "D")}`);
console.log(`E = ${nilaiFilterCount(hasil, "E")}`);
console.log(`F = ${nilaiFilterCount(hasil, "F")}`);
console.log("==================================");
